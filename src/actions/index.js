/**
 * Redux Actions
 */
export * from './ChatAppActions';
export * from './AppSettingsActions';
export * from './EmailAppActions';
export * from './TodoAppActions';
export * from './AuthActions';
export * from './FeedbacksActions';
export * from './EcommerceActions';
export * from './CrmActions';
export const test = () => (dispatch) => {
    setInterval(() => {
        dispatch({
            type: 'TEST'
        })
    }, 2000)
}
