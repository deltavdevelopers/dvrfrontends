/**
 * Auth Actions
 * Auth Action With Google, Facebook, Twitter and Github
 */
import firebase from 'firebase/app';
import 'firebase/auth';
import { NotificationManager } from 'react-notifications';
import {
   LOGIN_USER,
   LOGIN_USER_SUCCESS,
   LOGIN_USER_FAILURE,
   LOGOUT_USER,
   PROCEED_TO_UPDATE_PASSWORD,
   PASSWORD_RESET_SUCCESS,
   RESET_FORGOT_PASSWORD_STATE,
   PASSWORD_RESET_ERROR
   
} from 'Actions/types';
import Axios from 'axios'
import { memo } from 'react';

/**
 * inactivity duration in milli seconds.
 */
const inactivityTime = 60*60*1000;
var signoutTimer;

window.addEventListener('click',() => {
      window.clearTimeout(signoutTimer)
      signoutTimer = (localStorage.getItem('user_id') === null) ? null : window.setTimeout(() => {
            firebase.auth().signOut()
            .then(() => {
               localStorage.removeItem('user_id');
               NotificationManager.error('session timed out.');
               setTimeout(()=>{
                  location.reload()
               },2000)
            })
            .catch((error) => {
            })
      },inactivityTime)
})


/**
 * Redux Action To Sigin User With Firebase
 */
export const signinUserInFirebase = (user, history) => (dispatch) => {
   dispatch({ type: LOGIN_USER });
   firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION).then(()=>{
         return firebase.auth().signInWithEmailAndPassword(user.email, user.password)
   })
   .then(async (user) => {
      
         console.log("user got:",user);

         /**
          * add signout timer
          */
         window.clearTimeout(signoutTimer);
         signoutTimer = window.setTimeout(() => {
            firebase.auth().signOut()
            .then(() => {
               if(localStorage.getItem('user_id'))
               localStorage.removeItem('user_id');
               NotificationManager.error('session timed out.');
               setTimeout(()=>{
                  location.reload()
               },2000)
            })
            .catch((error) => {
            })
         },inactivityTime)

         localStorage.setItem("user_id",user.user.uid);
         await dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user_id') });
         history.push('/');
         NotificationManager.success('User Login Successfully!');
      })
      .catch((error) => {
         dispatch({ type: LOGIN_USER_FAILURE });

         switch (error.code) {
               case "auth/user-not-found":
                  NotificationManager.error("User records does not exists, please contact DeltaV Robotics to create new record");
                  break;
               case "auth/wrong-password":
                  NotificationManager.error("Invalid password, please click forgot password to reset your password");
                  break;
               default:
                  NotificationManager.error(error.message)
                     break;
         }
         
      });
}

/**
 * Redux Action to send forget password link
 */
export const emailPasswordResetLink = (email) => (dispatch) => {

      firebase.auth().sendPasswordResetEmail(email).then(function() {
            // Email sent.
            console.log("current user:",firebase.auth().currentUser);
            
            NotificationManager.success('Password reset link sent to your mail!');
          }).catch(function(error) {
            NotificationManager.error('Could not find your account');
          })
}
/**
 * Redux Action To Signout User From  Firebase
 */
export const logoutUserFromFirebase = () => (dispatch) => {
   firebase.auth().signOut()
      .then(() => {
         dispatch({ type: LOGOUT_USER });
         localStorage.removeItem('user_id');
         NotificationManager.success('User Logout Successfully');
      })
      .catch((error) => {
         NotificationManager.error(error.message);
      })
}

/**
 * Redux Action to send password reset email
 */
export const emailPasswordResetOtp = (email) => (dispatch) => {
      // firebase.functions.httpsCallable('sendPasswordResetEmail')({email})
      Axios.post('https://us-central1-react-project-1555f.cloudfunctions.net/sendPasswordResetEmail',{
            email
      })
      .then((result) => {
            const message = result.data
            console.log("got message:", message);
            
            dispatch({ type: PROCEED_TO_UPDATE_PASSWORD })
            NotificationManager.success(message || 'Password Reset otp is mailed.')
      })
      .catch(error => {
            console.log("got error:",error);
            
            const message = error.response ? error.response.data : 'Something went wrong! Please try again.'
            NotificationManager.error(message)
      })    
}

/**
 * Redux Action to reset password with otp.
 */
export const resetPasswordUsingOtp = (email, otp, password) => (dispatch) => {
      // firebase.functions().httpsCallable('resetPassword')
      Axios.post('https://us-central1-react-project-1555f.cloudfunctions.net/resetPassword',{
            email,
            otp,
            password
      })
      .then((result) => {
            const message = result.data;
            console.log("got re message:",result);
            
            dispatch({ type: PASSWORD_RESET_SUCCESS })
            NotificationManager.success(message || 'Password Reset Successfully.')
      })
      .catch(error => {
            console.log("got re error:",error);
            console.log("code",error.response);
            
            if(error && error.response && error.response.status == 401){
                  const message = error.response ? error.response.data : 'Could not Reset Password.'
                  NotificationManager.error(message)
            }else{
                  dispatch({ type: PASSWORD_RESET_ERROR })
                  const message = error.response ? error.response.data : 'Could not Reset Password.'
                  NotificationManager.error(message)
            }
      })
}

/**
 * Redux Action to reset forgot password state to initial
 */
export const resetForgotPasswordState = () => (dispatch) => {
      dispatch({
            type: RESET_FORGOT_PASSWORD_STATE
      })
} 

/**
 * Redux Action To Signup User In Firebase
 */
// export const signupUserInFirebase = (user, history) => (dispatch) => {
//    dispatch({ type: SIGNUP_USER });
//    firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
//       .then((success) => {
//          localStorage.setItem("user_id", "user-id");
//          dispatch({ type: SIGNUP_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//          history.push('/');
//          NotificationManager.success('Account Created Successfully!');
//       })
//       .catch((error) => {
//          dispatch({ type: SIGNUP_USER_FAILURE });
//          NotificationManager.error(error.message);
//       })
// }

// /**
//  * Redux Action To Signin User In Firebase With Facebook
//  */
// export const signinUserWithFacebook = (history) => (dispatch) => {
//    dispatch({ type: LOGIN_USER });
//    const provider = new firebase.auth.FacebookAuthProvider();
//    firebase.auth().signInWithPopup(provider).then(function (result) {
//       localStorage.setItem("user_id", "user-id");
//       dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//       history.push('/');
//       NotificationManager.success(`Hi ${result.user.displayName}!`);
//    }).catch(function (error) {
//       dispatch({ type: LOGIN_USER_FAILURE });
//       NotificationManager.error(error.message);
//    });
// }

// /**
//  * Redux Action To Signin User In Firebase With Google
//  */
// export const signinUserWithGoogle = (history) => (dispatch) => {
//    dispatch({ type: LOGIN_USER });
//    const provider = new firebase.auth.GoogleAuthProvider();
//    firebase.auth().signInWithPopup(provider).then(function (result) {
//       localStorage.setItem("user_id", "user-id");
//       dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//       history.push('/');
//       NotificationManager.success(`Hi ${result.user.displayName}!`);
//    }).catch(function (error) {
//       dispatch({ type: LOGIN_USER_FAILURE });
//       NotificationManager.error(error.message);
//    });
// }

// /**
//  * Redux Action To Signin User In Firebase With Github
//  */
// export const signinUserWithGithub = (history) => (dispatch) => {
//    dispatch({ type: LOGIN_USER });
//    const provider = new firebase.auth.GithubAuthProvider();
//    firebase.auth().signInWithPopup(provider).then(function (result) {
//       localStorage.setItem("user_id", "user-id");
//       dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//       history.push('/');
//       NotificationManager.success(`Hi ${result.user.displayName}!`);
//    }).catch(function (error) {
//       dispatch({ type: LOGIN_USER_FAILURE });
//       NotificationManager.error(error.message);
//    });
// }

// /**
//  * Redux Action To Signin User In Firebase With Twitter
//  */
// export const signinUserWithTwitter = (history) => (dispatch) => {
//    dispatch({ type: LOGIN_USER });
//    const provider = new firebase.auth.TwitterAuthProvider();
//    firebase.auth().signInWithPopup(provider).then(function (result) {
//       localStorage.setItem("user_id", "user-id");
//       dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user_id') });
//       history.push('/');
//       NotificationManager.success('User Login Successfully!');
//    }).catch(function (error) {
//       dispatch({ type: LOGIN_USER_FAILURE });
//       NotificationManager.error(error.message);
//    });
// }