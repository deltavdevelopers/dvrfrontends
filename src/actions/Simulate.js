import {
    SIMULATE_CHANGE_SIMPLE,
    SIMULATE_CHANGE_TAB,
    SIMULATE_CHANGE_AXIS,
    SIMULATE_CHANGE_UNIT,
    CREATE_SIMULATION_SUCCESS,
    SAVE_CZML_DATA,
    GET_SIMULATION_LIST,
    FILTER_SIMULATION_LIST,
    RESET_SIMULATION_LIST
} from './types';

import SimulateService from '../api/Simulate';

export const changeTab = (value) => ({
    type: SIMULATE_CHANGE_TAB,
    payload: value
});

export const changeSimple = (path, value) => ({
    type: SIMULATE_CHANGE_SIMPLE,
    payload: { path, value },
})

export const changeAxis = (which, value) => ({
    type: SIMULATE_CHANGE_AXIS,
    payload: {
        which,
        value
    }
});

export const changeUnit = (which, value) => ({
    type: SIMULATE_CHANGE_UNIT,
    payload: {
        which,
        value
    }
});

export const getSimulationList = () => async dispatch => {
    try {
        const response = await SimulateService.getAllSimulations()
        dispatch({type: GET_SIMULATION_LIST, payload: response})
    } catch (error) {
        console.log('error: ', error);
    }
}

export const filterSimulationList = (val) => ({type: FILTER_SIMULATION_LIST, payload: val})

export const resetSimulationList = () => ({type: RESET_SIMULATION_LIST})

export const saveCzmlData = data => ({type: SAVE_CZML_DATA, payload: data})