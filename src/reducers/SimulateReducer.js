import { fromJS } from 'immutable';
import {
    SIMULATE_CHANGE_TAB,
    SIMULATE_CHANGE_AXIS,
    SIMULATE_CHANGE_UNIT,
    SIMULATE_CHANGE_SIMPLE,
    CREATE_SIMULATION_SUCCESS,
    SAVE_CZML_DATA,
    GET_SIMULATION_LIST,
    FILTER_SIMULATION_LIST,
    RESET_SIMULATION_LIST
} from '../actions/types';

const _3D = {
    X: 1,
    Y: 1,
    Z: 1,
    XU: 'km',
    YU: 'km',
    ZU: 'km',
};

const dummyCase = {
    caseName: 'Foo',
    ProjectName: 'FooFoo',
    Start_Time: Date.now().toString(),
    End_Time: Date.now().toString(),
    Time_Formate: 'UTC'
};

const initState = fromJS({
    tabs: [
        { label: 'Parameters', value: 'p' },
        { label: 'Simulation', value: 's' }
    ],
    case: dummyCase,
    parameters: {
        OrbitType: {
            Position_initial: { ..._3D },
            velocity_initial: { ..._3D },
            SimulationConfiguration: {
                Integrator: {
                    type: '',
                    Initial_step_Size: '',
                    Accuracy: '',
                    Min_Step_size: '',
                    Max_Step_size: '',
                    Max_Step_Attempts: '',
                }
            },
            Forces: {
                GarviationalModel: {
                    Degree: '',
                    Order: '',
                    STM_Limit: '',
                    GravitationalModelName: '',
                    Tide: ''
                },
                DragForce: '',
                Solar_Radiation: '',
                Relatvistic_Correction: ''
            },
        },
        Attitude: {
            type: '',
            DCM: {},
            Mass: {
                value: '',
                Units: ''
            },
            MOI: {
                units: '',
                value: {
                    Ixx: '',
                    Ixy: '',
                    Ixz: '',
                    Iyx: '',
                    Iyy: '',
                    Iyz: '',
                    Izx: '',
                    Izy: '',
                    Izz: ''
                }
            }
        }
    },
    activeTab: 'p',
    czmlData: [],
    simulationsList: []
});

export default function (state = initState, action) {
    switch (action.type) {
        case SIMULATE_CHANGE_TAB: {
            return state.set('activeTab', action.payload);
        }
        case SIMULATE_CHANGE_AXIS: {
            const { which, value } = action.payload;
            return state.setIn(which, value);
        }
        case SIMULATE_CHANGE_UNIT: {
            const { which, value } = action.payload;
            return state.setIn(which, value);
        }
        case SIMULATE_CHANGE_SIMPLE: {
            const { path, value } = action.payload;
            return state.setIn(path, value);
        }
        case CREATE_SIMULATION_SUCCESS: {
            return initState;
        }
        case SAVE_CZML_DATA: {
            const czmlData = [...state.czmlData];
            czmlData.push(action.payload)
            return {
                ...state,
                czmlData
            }
        }
        case GET_SIMULATION_LIST:
            return state
                .set('simulationsList', action.payload)
                .set('filteredSimulationList', action.payload)

        case FILTER_SIMULATION_LIST: {
            const value = action.payload
            const list = state.get('simulationsList')
            const newList = list.filter(l => l.ProjectName.match(value))
            return state.set('filteredSimulationList', newList)
        }
        case RESET_SIMULATION_LIST: {
            const list = state.get('simulationsList')
            return state.set("filteredSimulationList", list)
        }
        default: {
            return state;
        }
    }
}
