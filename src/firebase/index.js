/**
 * Firebase Login
 * Reactify comes with built in firebase login feature
 * You Need To Add Your Firsebase App Account Details Here
 */
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/database';

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyBTgSkIZUfTm-M0akdhbyB5biUfxze-8A0",
    authDomain: "react-project-1555f.firebaseapp.com",
    databaseURL: "https://react-project-1555f.firebaseio.com",
    projectId: "react-project-1555f",
    storageBucket: "react-project-1555f.appspot.com",
    messagingSenderId: "482105859840",
    appId: "1:482105859840:web:b2b063ae775a316d"
  };

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

// const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
// const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
// const githubAuthProvider = new firebase.auth.GithubAuthProvider();
// const twitterAuthProvider = new firebase.auth.TwitterAuthProvider();
const database = firebase.database();
const fireStore = firebase.firestore();

export {
   auth,
//    googleAuthProvider,
//    githubAuthProvider,
//    facebookAuthProvider,
//    twitterAuthProvider,
   database,
   fireStore
};
