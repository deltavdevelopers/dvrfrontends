import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import moment from 'moment';
import { Card, TextField, Button, Radio } from "@material-ui/core";

import * as actions from '../../actions/Simulate';
import { withStyles } from '@material-ui/styles';
import './simulationList.scss'

const style = theme => ({
    searchCard: {
        height: '60px'
    },
    textField: {
        width: '90%',
        margin: '0 15px'
    },

})
class SimulationsList extends React.Component {
    constructor() {
        super()
        this.searchData = React.createRef();
        this.state = {
            selected: ''
        }
    }
    componentDidMount() {
        this.props.getSimulationList()
    }
    filterSimulationList = () => {
        const { value } = this.searchData.current;
        // if (!value) {
        //     this.props.resetSimulationList()
        //     return
        // }
        // this.props.filterSimulationList(value)
        clearTimeout(this.filterSimulationList)
        setTimeout(() => {
            console.log('value', value);
        }, 2000)
    }
    render() {
        const { selected } = this.state
        const columns = [
            {
                Header: "Select",
                Cell: row => <Radio
                    type="checkbox"
                    color="primary"
                    checked={selected === row.original.id}
                    onClick={() => this.setState({ selected: row.original.id })} />
            },
            {
                Header: "Project Name",
                accessor: 'ProjectName',
                // filter: textFilter()
            }, {
                Header: "Project Description",
                accessor: 'ProjectDescription'

            }, {
                Header: "Created At",
                accessor: 'CreatedDate',
                Cell: row => <div>{moment(row.original.CreatedDate).format('MMM Do YYYY, h:mm:ss a')}</div>

            }, {
                Header: "Last Modified Date",
                accessor: 'LastModifiedDate',
                Cell: row => <div>{moment(row.original.LastModifiedDate).format('MMM Do YYYY, h:mm:ss a')}</div>

            }
        ]
        const filteredSimulationList = this.props.simulate.get('filteredSimulationList')
        if (!(Array.isArray(filteredSimulationList) && filteredSimulationList.length > 0)) return null
        const { classes } = this.props
        return (
            <React.Fragment>
                <div>
                    <Card className={classes.searchCard}>
                        <div className="flex simulation_search">
                            <h3>Search</h3>
                            <TextField
                                className={classes.textField}
                                margin="none"
                                inputRef={this.searchData}
                                onKeyDown={(e) => e.which == 13 && this.filterSimulationList()} />
                            <Button variant="contained" color="primary" onClick={this.filterSimulationList}>Search</Button>
                        </div>
                    </Card>
                </div>
                <div class="simulation_table_list">
                    <div className="simulation_list_title">Simulation List</div>
                    <Card >
                        <ReactTable
                            data={filteredSimulationList}
                            columns={columns}
                            noDataText="No Simulations Found !"
                            defaultPageSize="5"
                            sort
                        />
                    </Card>
                </div>
            </React.Fragment>
        )
    }
}

const mapS2P = state => ({
    simulate: state.simulate
})
export default connect(mapS2P, actions)(withStyles(style)(SimulationsList))
