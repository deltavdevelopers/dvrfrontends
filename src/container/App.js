/**
 * App.js Layout Start Here
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';

// rct theme provider
import RctThemeProvider from './RctThemeProvider';

//Horizontal Layout
import HorizontalLayout from './HorizontalLayout';

//Agency Layout
import AgencyLayout from './AgencyLayout';

//Main App
import RctDefaultLayout from './DefaultLayout';

// boxed layout
import RctBoxedLayout from './RctBoxedLayout';
// CRM layout
import CRMLayout from './CRMLayout';

// app signin
import AppSignIn from './SigninFirebase';
// Forgot Password
import ForgotPassword from './ForgotPassword';

import * as actions from '../actions/AuthActions'
/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const InitialPath = ({ component: Component, ...rest }) =>
   <Route
      {...rest}
      render={props => <Component {...props} />}
   />;

class App extends Component {
   render() {
      const { location, match, user } = this.props;
      const usr = localStorage.getItem('user_id')
      if (location.pathname != '/signin' && location.pathname != '/forgot-password') {
        if (user === null || usr === null) {
            return (<Redirect to={'/signin'} />);
         } 
      //    else {
      //       return (<Redirect to={'/app/dashboard/ecommerce'} />);
      //    }
      }
      if(location.pathname == '/'){
        return (<Redirect to={'/app/dashboard/ecommerce'} />);
      }
      return (
         <RctThemeProvider>
            <NotificationContainer />
            <InitialPath
               path={`${match.url}app`}
               authUser={user}
               component={RctDefaultLayout}
            />
            <Route path="/signin" component={AppSignIn} />
            <Route path="/forgot-password" component={ForgotPassword} />
            <Route path="/horizontal" component={HorizontalLayout} />
            <Route path="/agency" component={AgencyLayout} />
            <Route path="/boxed" component={RctBoxedLayout} />
            <Route path="/dashboard" component={CRMLayout} />
         </RctThemeProvider>
      );
   }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
   const { user } = authUser;
   return { user };
};

export default connect(mapStateToProps, actions)(App);
