import React, { useState, useEffect } from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import styled from 'styled-components';
import ReactSelect from 'react-select';
import './index.scss';
import SimulationService from './../../api/Simulate';
import { NotificationManager } from 'react-notifications';

const steps = [
    'Create / Select Project',
    'Create New Simulation'
];
const ProjectForm = styled.div`
    margin: 0 20px;
    width: 100%
`;

const StyledStep = styled.div`
    margin: 20px;
`;
const resetStartDate = new Date().toISOString().split('.')[0];
const resetEndDate = new Date(new Date().setMonth(new Date().getMonth()+1)).toISOString().split('.')[0];
const CreateSelect = ({
    createOrSelect,
    setCreateOrSelect,
    handleChange,
    ProjectName,
    e_name,
    handleExistingProjectChange,
    e_description,
    projectsList,
    setProjectId,
    ProjectDescription,
}) => (
        <div className="flex">
            <ProjectForm>
                <FormControlLabel
                    control={
                        <Radio
                            checked={createOrSelect === 'c'}
                            onChange={() => setCreateOrSelect('c')}
                        />
                    }
                    label="New Project"
                    labelPlacement="end"
                />
                <div className="project">
                    <TextField
                        label="Name"
                        value={ProjectName}
                        onChange={handleChange('ProjectName')}
                        classes={{
                            root: 'field'
                        }}
                        disabled={createOrSelect !== 'c'}
                    />
                    <TextField
                        label="Description"
                        multiline
                        rowsMax="4"
                        value={ProjectDescription}
                        onChange={handleChange('ProjectDescription')}
                        classes={{
                            root: 'field'
                        }}
                        disabled={createOrSelect !== 'c'}
                    />
                </div>
            </ProjectForm>
            <ProjectForm>
                <FormControlLabel
                    control={
                        <Radio
                            checked={createOrSelect === 's'}
                            onChange={() => setCreateOrSelect('s')}
                        />
                    }
                    label="Select Project"
                    labelPlacement="end"
                />
                <div className="selected_project">
                    <ReactSelect
                        value={e_name}
                        onChange={(e) => {
                            handleExistingProjectChange(e.value)
                        }}
                        placeholder="Select Project"
                        isDisabled={createOrSelect !== 's'}
                        options={projectsList.map(p => ({ label: p.ProjectName, value: p.id }))}
                    />
                    <TextField
                        label="Description"
                        value={e_description}
                        // multiline
                        // rowsMax="4"
                        classes={{
                            root: 'field'
                        }}
                        disabled
                    />
                </div>
            </ProjectForm>
        </div>
    );

const NewSimulation = ({
    simulation: {
        startDate,
        endDate,
        CaseName,
        CaseDescription
    },
    changeSimulation
}) => (
        <div className="simulation_flex">
            <TextField
                label="Name"
                value={CaseName}
                onChange={changeSimulation('CaseName')}
                classes={{
                    root: 'field'
                }}
            />
            <TextField
                label="Description"
                multiline
                rowsMax="4"
                value={CaseDescription}
                onChange={changeSimulation('CaseDescription')}
                classes={{
                    root: 'field'
                }}
            />
            <TextField
                id="start"
                label="Start Date"
                type="datetime-local"
                value={startDate}
                // defaultValue="2017-05-24T10:30"
                onChange={changeSimulation('startDate')}
                classes={{
                    root: 'field'
                }}
            />
            <TextField
                id="date"
                label="End Date"
                value={endDate}
                type="datetime-local"
                // defaultValue="2017-05-2T10:30"
                onChange={changeSimulation('endDate')}
                classes={{
                    root: 'field'
                }}
            />
        </div>
    );

const getContent = (index) => {
    switch (index) {
        case 0: {
            return CreateSelect;
        }
        case 1: {
            return NewSimulation;
        }
        default: {
            return CreateSelect;
        }
    }
};

const Simulation = () => {
    const [step, setStep] = useState(0);
    const [createOrSelect, setCreateOrSelect] = useState('c');
    const [project, setProject] = useState({
        ProjectName: '',
        ProjectDescription: '',
    });
    const [existingProject, setExistingProject] = useState({
        e_name: '',
        e_description: '',
    });
    const [selectedProjectId, setProjectId] = useState('')
    const [newProjectId, setNewProjectId] = useState('')
    const [simulation, setSimulation] = useState({
        CaseName: '',
        CaseDescription: '',
        CreatedDate: new Date().toISOString(),
        LastModifiedDate: new Date().toISOString(),
        startDate: resetStartDate,
        endDate: resetEndDate,
    });
    const [projectsList, setProjectList] = useState([])

    useEffect(() => {
        getAllSimulations()
    }, [])
    async function getAllSimulations() {
        const list = await SimulationService.getAllSimulations()
        setProjectList(list)
    }
    function handleBack() {
        setStep(step > 0 ? step - 1 : 0);
    }

    function changeSimulation(path) {
        return function (e) {
            setSimulation({
                ...simulation,
                [path]: e.target.value,
            })
        }
    }
    function resetAll() {
        setProjectId('')
        setNewProjectId('')
        setSimulation({
            CaseName: '',
            CaseDescription: '',
            startDate: resetStartDate,
            endDate: resetEndDate,
            CreatedDate: new Date().toISOString(),
            LastModifiedDate: new Date().toISOString()
        })
        setProject({
            ProjectName: '',
            ProjectDescription: '',
        })
        setExistingProject({
            e_name: '',
            e_description: ''
        })
        setStep(0)
    }
    async function processStep() {
        switch (step) {
            case 0: {
                if (createOrSelect === 's') break;
                const result = await SimulationService.createProject(project);
                getAllSimulations()
                NotificationManager.success("New Project Created")
                setNewProjectId(result.id)
                break;
            }
            case 1: {
                const id = createOrSelect === 's' ? selectedProjectId : newProjectId
                if (!simulationValidation()) break;
                await SimulationService.addSimulationInProject(id, simulation)
                NotificationManager.success("New Simulated Created !");
                resetAll()
                break;
            }
            default: {
                return;
            }
        }
        if (step < steps.length - 1)
            setStep(step + 1);
    }
    function projectValidation() {
        if (!project.ProjectName && !project.ProjectDescription) {
            NotificationManager.error("Enter All Fields!")
            return false
        }
        if (!project.ProjectName) {
            NotificationManager.error("Enter Project Name !")
            return false
        } else if (!project.ProjectDescription) {
            NotificationManager.error("Enter Project Description !")
            return false
        }
        const projectExists = projectsList.find(p => p.ProjectName === project.ProjectName)
        if (projectExists) {
            NotificationManager.error('Project Name Already Exists !')
            return false
        }
        return true
    }
    function simulationValidation() {
        const { CaseName, CaseDescription, startDate, endDate } = simulation;
        const sd = new Date(startDate), ed = new Date(endDate)
        if (!CaseName && !CaseDescription && !startDate && !endDate) {
            NotificationManager.error("Enter All Fields !")
            return false
        } else if (!CaseName) {
            NotificationManager.error("Enter Simulation Name")
            return false
        } else if (!CaseDescription) {
            NotificationManager.error("Enter Simuation Description")
            return false
        } else if (!startDate) {
            NotificationManager.error("Enter Simuation Start Date")
            return false
        } else if (!endDate) {
            NotificationManager.error("Enter Simuation End Date")
            return false
        } else if (sd > ed) {
            NotificationManager.error("Start date should be earlier than end date")
            return
        }
        return true
    }
    function handleNext() {
        if (step === 0) {
            switch (createOrSelect) {
                case 'c': {
                    if (!projectValidation()) return
                    break;
                }
                case 's': {
                    if (!selectedProjectId) {
                        NotificationManager.error("Select A Project !");
                        return;
                    }
                    break;
                }
                default:
                    return;
            }
        }
        processStep();

    }
    function handleExistingProjectChange(id) {
        if (!id) return
        setProjectId(id)
        const project = projectsList.find(p => p.id === id)
        setExistingProject({
            e_name: { label: project.ProjectName, value: id },
            e_description: project.ProjectDescription
        })
    }
    const stepProps = {
        createOrSelect,
        setCreateOrSelect,
        ...project,
        ...existingProject,
        handleChange: (what) => e => setProject({
            ...project,
            [what]: e.target.value
        }),
        simulation,
        handleExistingProjectChange,
        projectsList,
        changeSimulation
    };

    return (
        <Stepper activeStep={step} orientation="vertical">
            {steps.map((label, index) => (
                <Step key={index}>
                    <StepLabel>{label}</StepLabel>
                    <StepContent>
                        <StyledStep>
                            {
                                getContent(index)(stepProps)
                            }
                        </StyledStep>
                        <Button
                            color="primary"
                            variant="contained"
                            disabled={step === 0}
                            onClick={handleBack}
                        >
                            Back
                        </Button>
                        <Button
                            onClick={handleNext}
                            color="primary"
                            variant="contained"
                        >
                            {step === steps.length - 1 ? 'Submit' : 'Next'}
                        </Button>
                    </StepContent>
                </Step>
            ))}
        </Stepper>
    )
};

export default Simulation;