import { fireStore, auth } from '../firebase';

const Project = fireStore.collection('simulations')
const sendResponse = data => Promise.resolve(data)
const sendError = data => Promise.reject(data)
const uid = localStorage.getItem("user_id") || auth.currentUser.uid

export default class SimulationService {
    static async createProject(data) {
        try {
            let response = await Project.add({
                ProjectName: data.ProjectName,
                ProjectDescription: data.ProjectDescription,
                UserDocId: uid,
                CreatedDate: new Date().toISOString(),
                LastModifiedDate: new Date().toISOString()
            })
            return sendResponse(response)
        } catch (error) {
            sendError(error)
            console.log('error: ', error);
        }
    }

    static async addSimulationInProject(id, data) {
        try {
            const {
                CaseName,
                CaseDescription,
                startDate,
                endDate
            } = data
            await Project
                .doc(id)
                .collection('Case')
                .add({
                    CaseName,
                    CaseDescription,
                    startDate: new Date(startDate).toISOString(),
                    endDate: new Date(endDate).toISOString(),
                    CreatedDate: new Date().toISOString(),
                    LastModifiedDate: new Date().toISOString()
                })
        } catch (error) {
            console.log('error: ', error);
        }
    }

    static async getAllSimulations() {
        try {
            const snapshot = await Project.where("UserDocId", "==", uid).limit(50).get()
            const response = snapshot.docs.map(doc => ({
                id: doc.id, ...doc.data()
            }))
            return sendResponse(response);
        } catch (error) {
            console.log('error: ', error);
        }
    }
    
    static async searchSimulations() {
        try {

        } catch (error) {
            console.log('error: ', error);
        }
    }
}

