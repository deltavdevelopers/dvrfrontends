import openSocket from 'socket.io-client';
const Cesium = window.Cesium;
const SOCKET_URL = 'http://localhost:8080';

export default class Simulate {

    topic = '';
    socket = null;
    totalCzml = [];
    currentCzml = null;
    viewer = null;
    dataSource = null;

    constructor() {
        this.socket = openSocket(SOCKET_URL);
    }

    static initCesium(socket) {
        Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkM2I4YWM5OC00NTY3LTQ5ZjctODNlZC1lMDkxMTBhNmNhZDkiLCJpZCI6MTQzMDEsInNjb3BlcyI6WyJhc2wiLCJhc3IiLCJnYyJdLCJpYXQiOjE1NjU0MjAwNDR9.9FdWdRLNtn2FLXpdtKZDZQ3g0Zsq3ddYx1iNUU_EL-4';
        const viewer = new Cesium.Viewer('cesiumContainer');
        this.viewer = viewer;
        const dataSource = new Cesium.CzmlDataSource();
        this.dataSource = dataSource;
        viewer.dataSources.add(dataSource);
        socket.on('czml', message => {
            this.dataSource.process(JSON.parse(message.value));
        });
        return Promise.resolve(this.dataSource)
    }
}