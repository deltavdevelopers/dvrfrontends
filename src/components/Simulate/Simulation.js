import React, { useEffect, useState, Fragment } from 'react';
import { Row, Col } from 'reactstrap';
import { Line } from 'react-chartjs-2';
import { Button } from '@material-ui/core';
import SimulateUtil from '../../utils/simulate'

const Cesium = window.Cesium;

const data = {
    labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    datasets: [
        {
            label: 'Series B',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(120, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        },
        {
            label: 'Series A',
            data: [4, 10, 7, 8, 10, 6],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }
    ]
}

const options = {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }
};

const Simulation = (props) => {

    const [showChart, changeChart] = useState(true);
    const [viewer, setViewer] = useState(null);
    const { socket, saveCzmlData } = props;

    useEffect(() => {
        getCzmlData()
    }, []);

    async function getCzmlData() {
        let czml = await SimulateUtil.initCesium(socket);
        saveCzmlData(czml)
    }
    return (
        <Row>
            <Col md={showChart ? "8" : "12"} >
                <div id="cesiumContainer">
                    <div className="chart-switch">
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={() => changeChart(!showChart)}
                        >
                            Toggle Graph
                        </Button>
                    </div>
                </div>
            </Col>
            <Col md="4">
                {
                    showChart && (
                        <Fragment>
                            <Line
                                data={data}
                                options={options}
                            />
                            <Line
                                data={data}
                                options={options}
                            />
                        </Fragment>
                    )
                }
            </Col>
        </Row>
    )
}

export default Simulation;