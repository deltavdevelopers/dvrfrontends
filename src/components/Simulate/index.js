import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { saveCzmlData } from '../../actions/Simulate'
import Tab from './Tab';
import Parameters from './Parameters';
import Simulation from './Simulation';
import Simulator from '../../utils/simulate';
import './index.scss';

const Simulate = (props) => {
    const {
        isParameter,
        saveCzmlData
    } = props;

    const [socket, setSocket] = useState(null);

    useEffect(() => {
        const simulatorObject = new Simulator();
        setSocket(simulatorObject.socket);
    }, []);

    return (
        <div>
            <div className='simulate_title'>Simulate</div>
            <Tab />
            {
                isParameter ?
                    <Parameters socket={socket} /> :
                    <Simulation socket={socket} saveCzmlData={saveCzmlData}/>
            }
        </div>
    )
}

const mapStateToProps = state => ({
    isParameter: state.simulate.get('activeTab') === 'p'
});

const mapDispatchToProps = {
    saveCzmlData
}

export default connect(mapStateToProps, mapDispatchToProps)(Simulate);