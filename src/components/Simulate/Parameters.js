import React, { Component, Fragment } from 'react';
import InputFiles from 'react-input-files';
import { Link, withRouter } from 'react-router-dom'
import {
    TextField,
    Card,
    CardHeader,
    CardContent,
    Divider,
    Select,
    FormLabel,
    FormControl,
    FormControlLabel,
    InputLabel,
    Input,
    MenuItem,
    Grid,
    Button,
    Checkbox,
    Radio,
    RadioGroup,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails
} from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    changeAxis,
    changeUnit,
    createSimulation,
    changeSimple,
    changeTab
} from '../../actions/Simulate';

const useStyles = makeStyles(theme => ({
    headerTitle: {
        fontSize: '16px',
        color: '#444'
    },
    header: {
        backgroundColor: '#eeeeee'
    },
    card: {
        marginTop: '20px'
    },
    full: {
        width: '100%',
    },
    formControl: {
        minWidth: '200px',
        margin: '10px'
    },
    axisInput: {
        marginTop: '10px'
    },
    button: {
        margin: '15px 10px'
    }
}))

const Header = ({ title, styles }) => (
    <CardHeader title={title} classes={{ title: styles.headerTitle, root: styles.header }} />
)

const resolveValue = option => typeof option === 'string' ? option : option.value

const resolveLabel = option => typeof option === 'string' ? option : option.label

const CustomParameter = ({ id, label, path, unitPath, unitOptions, styles, value, unitValue, changeSimple }) => (
    <div className="d-flex">
        <TextField
            label={label}
            id={id}
            classes={{

                root: styles.axisInput
            }}
            value={value}
            onChange={e => changeSimple(path, e.target.value)}
        />
        <FormControl className={styles.formControl}>
            <InputLabel htmlFor={`${id}Unit`}>Unit</InputLabel>
            <Select
                value={unitValue}
                onChange={e => changeSimple(unitPath, e.target.value)}
                inputProps={{
                    name: `${id}Unit`,
                    id: `${id}Unit`,
                }}
            >
                {
                    unitOptions.map((option, index) =>
                        <MenuItem key={index} value={resolveValue(option)}>{ resolveLabel(option) }</MenuItem>
                    )
                }
            </Select>
        </FormControl>
    </div>
)
const CustomTextField = ({ id, label, value, changeSimple, path, type, styles, disabled, defaultValue }) => (
    <FormControl className={styles.formControl}>
        <TextField
            label={label}
            id={id}
            type={type || 'text'}
            classes={{
                root: styles.axisInput
            }}
            value={value}
            onChange={e => changeSimple(path, e.target.value)}
            disabled={disabled}
            defaultValue={defaultValue}
        />
    </FormControl>
)
const Axis = ({ axis, axisValue, axisChange, styles, unit, unitChange }) => (
    <div className="d-flex">
        <TextField
            label={`${axis}-axis`}
            id="x"
            classes={{
                root: styles.axisInput
            }}
            value={axisValue}
            onChange={e => axisChange(e.target.value)}
        />
        <FormControl className={styles.formControl}>

            <InputLabel htmlFor={`x`}>Unit</InputLabel>
            <Select
                value={unit}
                onChange={unitChange}
                inputProps={{
                    name: 'x',
                    id: 'x',
                }}
            >
                <MenuItem value='km'>Km</MenuItem>
                <MenuItem value='m'>meter</MenuItem>
            </Select>
        </FormControl>
    </div>
)

const makeOption = (label, value) => ({ label, value: value || label });

const algorithmTypes = [
    { label: 'SPG4', value: 'SPG4' },
    { label: 'Numerical Integration', value: 'NumericalIntegration' }
];

function Parameters(props) {

    const styles = useStyles();

    const { simulate, socket } = props;

    const ORBIT_PATH = ['parameters', 'OrbitType'];

    const POSITION_INIT = 'Position_initial';
    const VELOCITY_INIT = 'velocity_initial';
    const POSITION_PATH = [ ...ORBIT_PATH, POSITION_INIT ];
    const VELOCITY_PATH = [ ...ORBIT_PATH, VELOCITY_INIT ];
    const ATTR_PATH = ['parameters', 'Attitude'];
    const OrbitType = simulate.getIn(ORBIT_PATH);

    const init = OrbitType.get(POSITION_INIT);

    const velocity = OrbitType.get(VELOCITY_INIT);

    const {
        changeAxis,
        changeUnit,
        changeSimple,
    } = props;

    const CustomSelect = ({ id, label, path, options, disabled }) => (
        <FormControl className={styles.formControl}>
            <InputLabel htmlFor={id}>{label}</InputLabel>
            <Select
                value={simulate.getIn(path)}
                onChange={e => changeSimple(path, e.target.value)}
                inputProps={{
                    name: { id },
                    id: { id },
                }}
                disabled={disabled}
            >{
                    options.map((option, i) => (
                        <MenuItem value={option.value}>
                            { option.label }
                        </MenuItem>
                    ))
                }
            </Select>
        </FormControl>
    )

    const coordinateSystems = [
        { label: 'Cartesian', value: 'cartesian' },
        { label: 'Keplerian', value: 'keplerian' },
    ];

    const handleRun = async () => {
        if (!(simulate && simulate.toJS))
            return
        try{
            const data = simulate.get('parameters').toJS();
            const { value: MOIValue } = data.Attitude.MOI;
            const { DCM } = data.Attitude;
            data.Attitude.MOI.value = Object.keys(MOIValue).sort().map(key => MOIValue[key]);
            data.Attitude.DCM = [
                [DCM[0], DCM[1], DCM[2]],
                [DCM[3], DCM[4], DCM[5]],
                [DCM[6], DCM[7], DCM[8]]
            ];
            const { Position_initial, velocity_initial } = data.OrbitType;
            data.OrbitType.Position_initial = {
                x: {
                    unit: Position_initial.XU,
                    value: Position_initial.X
                },
                y: {
                    unit: Position_initial.YU,
                    value: Position_initial.Y
                },
                z: {
                    unit: Position_initial.ZU,
                    value: Position_initial.Z
                }
            };
            data.OrbitType.velocity_initial = {
                x: {
                    unit: velocity_initial.XU,
                    value: velocity_initial.X
                },
                y: {
                    unit: velocity_initial.YU,
                    value: velocity_initial.Y
                },
                z: {
                    unit: velocity_initial.ZU,
                    value: velocity_initial.Z
                }
            };
            data.Command = 'Start';
            data.case = simulate.get('case').toJS();
            console.log('data now : ', data);
            // await createSimulation(data);
            socket.emit('simulation', data);
            props.changeTab('s');
        }
        catch(err) {
            console.log(err);
        }
    }

    const isNumerical = simulate.getIn([ ...ORBIT_PATH, 'algorithmType']) === 'NumericalIntegration';

    const isKeplar = simulate.getIn([ ...ORBIT_PATH, 'StateType']) === 'keplerian';

    const gravitationalModelName = simulate.getIn([ ...ORBIT_PATH, 'Forces', 'GravitationalModel', 'GravitationalModelName']);

    return (
        <div>
            {/* Orbit */}
            <ExpansionPanel classes={{ root: styles.card }}>
                <ExpansionPanelSummary
                    classes={{ root: styles.header }}
                    expandIcon={<ExpandMore />}
                >
                    <div className={styles.full}>
                        <Header title="Orbit" styles={styles} />
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className={styles.full}>
                        <CustomSelect
                            id="algoType"
                            label="Algorithm Type"
                            path={['parameters', 'OrbitType', 'algorithmType']}
                            options={algorithmTypes}
                        />

                        {
                            simulate.getIn(['parameters', 'OrbitType', 'algorithmType']) === 'SPG4' &&
                            <Fragment>
                                <InputFiles onChange={files => console.log(files)}>
                                    <Button color="primary" mt={"1rem"} variant="contained">Upload spga config File</Button>
                                </InputFiles>
                            </Fragment>
                        }

                        {
                            isNumerical &&
                            <Fragment>
                                <CustomSelect
                                    id="coord"
                                    label="coordinate Systems"
                                    path={['parameters', 'OrbitType', 'CoordinationType']}
                                    options={[
                                        { label: 'Earth Center EathFixed(ECEF)', value: 'ECEF' },
                                        { label: 'Earth International Celestial Reference Frame', value: 'EICRF' },
                                        { label: 'EarthMJ2000Eq', value: 'EMEQ' },
                                        { label: 'EarthMJ2000EC', value: 'EMEC' }
                                    ]}
                                />
                                <CustomSelect
                                    id="stateType"
                                    label="State Type"
                                    path={['parameters', 'OrbitType', 'StateType']}
                                    options={coordinateSystems}
                                />
                            </Fragment>
                        }
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>

            {
                isNumerical &&
                <ExpansionPanel classes={{ root: styles.card }}>
                    <ExpansionPanelSummary
                        classes={{ root: styles.header }}
                        expandIcon={<ExpandMore />}
                    >
                        <div className={styles.full}>
                            <Header title="Initial Position and Velocity in Space" styles={styles} />
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className={styles.full}>
                            <Grid container>
                                <Grid item md={6}>
                                    <Axis
                                        axis="X"
                                        axisValue={init.get('X')}
                                        axisChange={e => changeAxis([...POSITION_PATH, 'X'], e)}
                                        unit={init.get('XU')}
                                        unitChange={e => changeUnit([...POSITION_PATH, 'XU'], e.target.value)}
                                        styles={styles} />
                                    <Axis
                                        axis="Y"
                                        axisValue={init.get('Y')}
                                        axisChange={e => changeAxis([...POSITION_PATH, 'Y'], e)}
                                        unit={init.get('YU')}
                                        unitChange={e => changeUnit([...POSITION_PATH, 'YU'], e.target.value)}
                                        styles={styles}
                                    />
                                    <Axis
                                        axis="Z"
                                        axisValue={init.get('Z')}
                                        axisChange={e => changeAxis([...POSITION_PATH, 'Z'], e)}
                                        unit={init.get('ZU')}
                                        unitChange={e => changeUnit([...POSITION_PATH, 'ZU'], e.target.value)}
                                        styles={styles}
                                    />
                                </Grid>
                                <Grid item md={6}>
                                    <Axis
                                        axis="VX"
                                        axisValue={velocity.get('X')}
                                        axisChange={e => changeAxis([ ...VELOCITY_PATH, 'X'], e)}
                                        unit={velocity.get('XU')}
                                        unitChange={e => changeUnit([ ...VELOCITY_PATH, 'XU'], e.target.value)}
                                        styles={styles}
                                    />
                                    <Axis
                                        axis="VY"
                                        axisValue={velocity.get('Y')}
                                        axisChange={e => changeAxis([ ...VELOCITY_PATH, 'Y'], e)}
                                        unit={velocity.get('YU')}
                                        unitChange={e => changeUnit([ ...VELOCITY_PATH, 'YU'], e.target.value)}
                                        styles={styles}
                                    />
                                    <Axis
                                        axis="VZ"
                                        axisValue={velocity.get('Z')}
                                        axisChange={e => changeAxis([ ...VELOCITY_PATH, 'Z'], e)}
                                        unit={velocity.get('ZU')}
                                        unitChange={e => changeUnit([ ...VELOCITY_PATH, 'ZU'], e.target.value)}
                                        styles={styles}
                                    />
                                </Grid>
                            </Grid>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            }

            { isNumerical && isKeplar &&
                <ExpansionPanel classes={{ root: styles.card }}>
                    <ExpansionPanelSummary
                        classes={{ root: styles.header }}
                        expandIcon={<ExpandMore />}
                    >
                        <div className={styles.full}>
                            <Header title="Initial Position and Velocity in Space" styles={styles} />
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className={styles.full}>
                            <Grid container>
                                <Grid item md={6}>
                                    <CustomParameter
                                        label='Semi-Mazor-axis'
                                        id="semiMazor"
                                        path={['parameters', 'init', 'semiMazor']}
                                        unitPath={['parameters', 'init', 'semiMazorUnit']}
                                        unitOptions={['Km', 'meters']}
                                        styles={styles}
                                        changeSimple={changeSimple}
                                        value={simulate.getIn(['parameters', 'init', 'semiMazor'])}
                                        unitValue={simulate.getIn(['parameters', 'init', 'semiMazorUnit'])}
                                    />
                                    <FormControl className={styles.formControl}>

                                        <CustomTextField
                                            label='Eccentricity'
                                            id="eccentricity"
                                            path={['parameters', 'init', 'eccentricity']}
                                            value={simulate.getIn(['parameters', 'init', 'eccentricity'])}
                                            changeSimple={changeSimple}
                                            type="number"
                                            styles={styles}
                                        />
                                        <CustomParameter
                                            label='Inclination'
                                            id="inclination"
                                            path={['parameters', 'init', 'inclination']}
                                            unitPath={['parameters', 'init', 'inclinationUnit']}
                                            unitOptions={['deg', 'radian']}
                                            styles={styles}
                                            changeSimple={changeSimple}
                                            value={simulate.getIn(['parameters', 'init', 'inclination'])}
                                            unitValue={simulate.getIn(['parameters', 'init', 'inclinationUnit'])}
                                        />
                                    </FormControl>

                                </Grid>
                                <Grid item md={6}>
                                    <CustomParameter
                                        label='RAAN'
                                        id="raan"
                                        path={['parameters', 'init', 'raan']}
                                        unitPath={['parameters', 'init', 'raanUnit']}
                                        unitOptions={['deg', 'radian']}
                                        styles={styles}
                                        changeSimple={changeSimple}
                                        value={simulate.getIn(['parameters', 'init', 'raan'])}
                                        unitValue={simulate.getIn(['parameters', 'init', 'raanUnit'])}
                                    />
                                    <CustomParameter
                                        label='AOP'
                                        id="aop"
                                        path={['parameters', 'init', 'aop']}
                                        unitPath={['parameters', 'init', 'aopUnit']}
                                        unitOptions={['deg', 'radian']}
                                        styles={styles}
                                        changeSimple={changeSimple}
                                        value={simulate.getIn(['parameters', 'init', 'aop'])}
                                        unitValue={simulate.getIn(['parameters', 'init', 'aopUnit'])}
                                    />
                                    <CustomParameter
                                        label='True Anomaly'
                                        id="trueAnomaly"
                                        path={['parameters', 'init', 'trueAnomaly']}
                                        unitPath={['parameters', 'init', 'trueAnomalyUnit']}
                                        unitOptions={['deg', 'radian']}
                                        styles={styles}
                                        changeSimple={changeSimple}
                                        value={simulate.getIn(['parameters', 'init', 'trueAnomaly'])}
                                        unitValue={simulate.getIn(['parameters', 'init', 'trueAnomalyUnit'])}
                                    />
                                </Grid>
                            </Grid>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            }


            <ExpansionPanel classes={{ root: styles.card }}>
                <ExpansionPanelSummary
                    classes={{ root: styles.header }}
                    expandIcon={<ExpandMore />}
                >
                    <div className={styles.full}>
                        <Header title="Simulation Configuration" styles={styles} />
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className={styles.full}>
                        <Grid container>
                            <Grid item md={4}>
                                <CustomSelect
                                    id="intergratorType"
                                    label="Integrator Type"
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'type']}
                                    options={[
                                        { label: "RungeKutta89", value: 'rk89' },
                                        { label: "RungeKutta68", value: 'rk68' },
                                        { label: "RungeKutta56", value: 'rk56' },
                                        { label: "AdamsBashMoulton", value: 'abm' },
                                    ]}
                                />
                            </Grid>
                            <Grid item md={4} >
                                <CustomTextField
                                    id="initStepSize"
                                    label="Initial Step Size"
                                    value={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'Initial_step_Size'])}
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'Initial_step_Size']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                            </Grid>
                            <Grid item md={4} >
                                <CustomTextField
                                    id="accuracy"
                                    label="Accuracy"
                                    value={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'Accuracy'])}
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'Accuracy']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                            </Grid>
                            <Grid item md={4} >
                                <CustomTextField
                                    id="minStepSize"
                                    label="Minimum Step Size"
                                    value={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'Accuracy', 'Min_Step_size'])}
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'Accuracy', 'Min_Step_size']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                            </Grid>
                            <Grid item md={4} >
                                <CustomTextField
                                    id="maxStepSize"
                                    label="Maximum Step Size"
                                    value={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'Max_Step_size'])}
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'Max_Step_size']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                            </Grid>
                            <Grid item md={4} >
                                <CustomTextField
                                    id="maxStepAttempts"
                                    label="Maximum Step Attempts"
                                    value={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'Max_Step_Attempts'])}
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'Max_Step_Attempts']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                            </Grid>

                            <Grid item md={4}>
                                <CustomTextField
                                    id="minimumError"
                                    label="Minimum Integration Error"
                                    value={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'Minimum_Error'])}
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'Minimum_Error']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                    disabled={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'type']) !== 'abm'}
                                />
                            </Grid>
                            <Grid item md={4}>
                                <CustomTextField
                                    id="nominalError"
                                    label="Nominal Integration Error"
                                    value={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'Nominal_Error'])}
                                    path={['parameters', 'SimulationConfiguration', 'Integrator', 'Nominal_Error']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                    disabled={simulate.getIn(['parameters', 'SimulationConfiguration', 'Integrator', 'type']) !== 'abm'}
                                />
                            </Grid>
                            <Grid item md={4} >
                                <FormControl styles={styles.formControl}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                id="stopIfAccViolated"
                                                checked={simulate.getIn(['parameters', 'SimulationConfiguration', 'stopIfAccViolated'])}
                                                onChange={e => changeSimple(['parameters', 'SimulationConfiguration', 'stopIfAccViolated'], e.target.checked)}
                                            />
                                        }
                                        label="Stop If Accuracy Violated"
                                    />
                                </FormControl>

                            </Grid>


                        </Grid >
                    </div >
                </ExpansionPanelDetails >
            </ExpansionPanel >

            <ExpansionPanel classes={{ root: styles.card }}>
                <ExpansionPanelSummary
                    classes={{ root: styles.header }}
                    expandIcon={<ExpandMore />}
                >
                    <div className={styles.full}>
                        <Header title="Force Configuration" styles={styles} />
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className={styles.full}>
                        <Grid container>
                            <Grid item md={2}>
                                <CustomSelect
                                    id="gravitationalModel"
                                    label="Gravitational Model"
                                    path={['parameters', 'OrbitType', 'Forces', 'GravitationalModel', 'GravitationalModelName']}
                                    options={[
                                        makeOption("None"),
                                        makeOption("JGM-2"),
                                        makeOption("JGM-3"),
                                        makeOption("EGM-3")
                                    ]}
                                />
                            </Grid>
                            <Grid item md={2} >
                                <CustomTextField
                                    id="degree"
                                    label="Degree"
                                    value={simulate.getIn(['parameters', 'OrbitType', 'Forces', 'Degree'])}
                                    path={['parameters', 'OrbitType', 'Forces', 'Degree']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                    defaultValue={4}
                                    disabled={
                                        !gravitationalModelName ||
                                        gravitationalModelName === 'JGM-2'
                                    }
                                />
                            </Grid>
                            <Grid item md={2} >
                                <CustomTextField
                                    id="order"
                                    label="Order"
                                    value={simulate.getIn(['parameters', 'OrbitType', 'Forces', 'Order'])}
                                    path={['parameters', 'OrbitType', 'Forces', 'Order']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                    defaultValue={4}
                                    disabled={
                                        !gravitationalModelName ||
                                        gravitationalModelName === 'JGM-2'
                                    }
                                />
                            </Grid>
                            <Grid item md={2} >
                                <CustomTextField
                                    id="stmLimit"
                                    label="STM Limit"
                                    value={simulate.getIn(['parameters', 'OrbitType', 'Forces', 'STM_Limit'])}
                                    path={['parameters', 'OrbitType', 'Forces', 'STM_Limit']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                    defaultValue={100}
                                    disabled={
                                        !gravitationalModelName ||
                                        gravitationalModelName === 'JGM-2'
                                    }
                                />
                            </Grid>

                            <Grid item md={2}>
                                <CustomSelect
                                    id="tide"
                                    label="Tide"
                                    path={['parameters', 'OrbitType', 'Forces', 'Tide']}
                                    options={[
                                        makeOption("None"),
                                        makeOption("Solid"),
                                        makeOption("Solid Pole")
                                    ]}
                                    disabled={
                                        !gravitationalModelName ||
                                        gravitationalModelName === 'JGM-2'
                                    }
                                />
                            </Grid>

                            <Divider />

                            <Grid item md={3} >
                                <CustomSelect
                                    id="drag"
                                    label="Drag"
                                    path={['parameters', 'OrbitType', 'Forces', 'DragForce']}
                                    options={[
                                        makeOption("None"),
                                        makeOption("MSISE90"),
                                        makeOption("JacchiaRoberts")
                                    ]}
                                />
                            </Grid>

                            <Grid item md={3}>
                                <FormControl className={styles.formControl}>
                                    <InputLabel htmlFor="thirdBodyForce">Third Body Force</InputLabel>
                                    <Select
                                        value={simulate.getIn(['parameters', 'OrbitType', 'Forces', 'Thirdbody']) || []}
                                        onChange={e => {
                                            console.log('e target : ', e.target);
                                            changeSimple(['parameters', 'OrbitType', 'Forces', 'Thirdbody'], e.target.value)
                                        }}
                                        inputProps={{
                                            name: "thirdBodyForce",
                                            id: "thirdBodyForce",
                                        }}
                                        multiple
                                    >{
                                        [
                                            "Sun",
                                            "Moon",
                                            "Mercury",
                                            "Venus",
                                            "Mars",
                                            "Jupiter",
                                            "Saturn",
                                            "Neptune",
                                            "Pluto",
                                            "None"
                                        ].map((option, i) => (
                                                <MenuItem key={i+1} value={option}>{option}</MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item md={3}>
                                <FormControl styles={styles.formControl}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                id="solarRadPressure"
                                                checked={simulate.getIn(['parameters', 'OrbitType', 'Forces', 'Solar_Radiation'])}
                                                onChange={e => changeSimple(['parameters', 'OrbitType', 'Forces', 'Solar_Radiation'], e.target.checked)}
                                            />
                                        }
                                        label="Solar Radition Pressure"
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item md={3} >
                                <FormControl styles={styles.formControl}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                id="relativisticCorrection"
                                                checked={simulate.getIn(['parameters', 'OrbitType', 'Forces', 'Relatvistic_Correction'])}
                                                onChange={e => changeSimple(['parameters', 'OrbitType', 'Forces', 'Relatvistic_Correction'], e.target.checked)}
                                            />
                                        }
                                        label="Relativistic Correction"
                                    />
                                </FormControl>

                            </Grid>
                        </Grid >
                    </div >
                </ExpansionPanelDetails >
            </ExpansionPanel >

            <ExpansionPanel classes={{ root: styles.card }}>
                <ExpansionPanelSummary
                    classes={{ root: styles.header }}
                    expandIcon={<ExpandMore />}
                >
                    <div className={styles.full}>
                        <Header title="Attitude" styles={styles} />
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className={styles.full}>
                        <Grid container>
                            <Grid item md={12}>
                                <CustomSelect
                                    id="bodyFrame"
                                    label="Body Frame w.r.t"
                                    path={[ ...ATTR_PATH, 'type']}
                                    options={[
                                        makeOption("ECI"),
                                        makeOption("ECEF")
                                    ]}
                                />
                                <CustomParameter
                                    label='Angular Vel-X'
                                    id="angularVelocityX"
                                    path={[ ...ATTR_PATH, 'angularVelocityX']}
                                    unitPath={[...ATTR_PATH, 'angularVelocityXUnit']}
                                    unitOptions={['deg/sec', 'radian/sec']}
                                    styles={styles}
                                    changeSimple={changeSimple}
                                    value={simulate.getIn([ ...ATTR_PATH, 'angularVelocityX'])}
                                    unitValue={simulate.getIn([ ...ATTR_PATH, 'angularVelocityXUnit'])}
                                />
                                <CustomParameter
                                    label='Angular Vel-Y'
                                    id="angularVelocityY"
                                    path={[ ...ATTR_PATH, 'angularVelocityY']}
                                    unitPath={[ ...ATTR_PATH, 'angularVelocityYUnit']}
                                    unitOptions={['deg/sec', 'radian/sec']}
                                    styles={styles}
                                    changeSimple={changeSimple}
                                    value={simulate.getIn([ ...ATTR_PATH, 'angularVelocityY'])}
                                    unitValue={simulate.getIn([ ...ATTR_PATH, 'angularVelocityYUnit'])}
                                />
                                <CustomParameter
                                    label='Angular Vel-Z'
                                    id="angularVelocityZ"
                                    path={[ ...ATTR_PATH, 'angularVelocityZ']}
                                    unitPath={[ ...ATTR_PATH, 'angularVelocityZUnit']}
                                    unitOptions={['deg/sec', 'radian/sec']}
                                    styles={styles}
                                    changeSimple={changeSimple}
                                    value={simulate.getIn([ ...ATTR_PATH, 'angularVelocityZ'])}
                                    unitValue={simulate.getIn([ ...ATTR_PATH, 'angularVelocityZUnit'])}
                                />
                            </Grid>
                            <Grid item md={12} >
                                <FormControl component="fieldset">
                                    <RadioGroup
                                        aria-label="attitudeRadio"
                                        name="attitudeRadio"
                                        value={simulate.getIn([ ...ATTR_PATH, 'attitudeRadio'])}
                                        onChange={e => changeSimple([ ...ATTR_PATH, 'attitudeRadio'], e.target.value)}
                                        row
                                    >
                                        <FormControlLabel
                                            value="quaternion"
                                            control={<Radio color="primary" />}
                                            label="Quaternion"
                                            labelPlacement="top"
                                        />
                                        <FormControlLabel
                                            value="eulerAngle"
                                            control={<Radio color="primary" />}
                                            label="Euler Angle"
                                            labelPlacement="top"
                                        />
                                        <FormControlLabel
                                            value="directionCosineMatrix"
                                            control={<Radio color="primary" />}
                                            label="Direction Cosine Matrix"
                                            labelPlacement="top"
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                            {
                                simulate.getIn(['parameters', 'Attitude', 'attitudeRadio']) === "quaternion" &&
                                <Fragment>
                                    <Grid item md={3} >
                                        <CustomTextField
                                            id="q0"
                                            label="Q0"
                                            value={simulate.getIn([ ...ATTR_PATH, 'q0'])}
                                            path={[ ...ATTR_PATH, 'q0']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={3} >
                                        <CustomTextField
                                            id="q1"
                                            label="Q1"
                                            value={simulate.getIn([ ...ATTR_PATH, 'q1'])}
                                            path={[ ...ATTR_PATH, 'q1']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={3} >
                                        <CustomTextField
                                            id="q2"
                                            label="Q2"
                                            value={simulate.getIn([ ...ATTR_PATH, 'q2'])}
                                            path={[ ...ATTR_PATH, 'q2']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={3} >
                                        <CustomTextField
                                            id="q3"
                                            label="Q3"
                                            value={simulate.getIn([ ...ATTR_PATH, 'q3'])}
                                            path={[ ...ATTR_PATH, 'q3']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                </Fragment>
                            }
                            {
                                simulate.getIn(['parameters', 'Attitude', 'attitudeRadio']) === "eulerAngle" &&
                                <Fragment>
                                    <Grid item md={3} >
                                        <CustomSelect
                                            id="eulerAngleSequence"
                                            label="Euler Angle Sequence"
                                            path={[ ...ATTR_PATH, 'eulerAngleSequence']}
                                            options={[
                                                "312",
                                                "123",
                                                "231"
                                            ]}
                                        />
                                    </Grid>
                                    <Grid item md={3} >
                                        <CustomTextField
                                            id="eulerAngle1"
                                            label="Euler Angle 1"
                                            value={simulate.getIn([ ...ATTR_PATH, 'eulerAngle1'])}
                                            path={[ ...ATTR_PATH, 'eulerAngle1']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={3} >
                                        <CustomTextField
                                            id="eulerAngle2"
                                            label="Euler Angle 2"
                                            value={simulate.getIn([ ...ATTR_PATH, 'eulerAngle2'])}
                                            path={[ ...ATTR_PATH, 'eulerAngle2']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={3} >
                                        <CustomTextField
                                            id="eulerAngle3"
                                            label="Euler Angle 3"
                                            value={simulate.getIn([ ...ATTR_PATH, 'eulerAngle3'])}
                                            path={[ ...ATTR_PATH, 'eulerAngle3']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                </Fragment>
                            }
                            {
                                simulate.getIn(['parameters', 'Attitude', 'attitudeRadio']) === "directionCosineMatrix" &&
                                <Fragment>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm1"
                                            label="DCM 1"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '0'])}
                                            path={[ ...ATTR_PATH, 'DCM', '0']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm2"
                                            label="DCM 2"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '1'])}
                                            path={[ ...ATTR_PATH, 'DCM', '1']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm3"
                                            label="DCM 3"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '2'])}
                                            path={[ ...ATTR_PATH, 'DCM', '2']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm4"
                                            label="DCM 4"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '3'])}
                                            path={[ ...ATTR_PATH, 'DCM', '3']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm5"
                                            label="DCM 5"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '4'])}
                                            path={[ ...ATTR_PATH, 'DCM', '4']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm6"
                                            label="DCM 6"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '5'])}
                                            path={[ ...ATTR_PATH, 'DCM', '5']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm7"
                                            label="DCM 7"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '6'])}
                                            path={[ ...ATTR_PATH, 'DCM', '6']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm8"
                                            label="DCM 8"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '7'])}
                                            path={[ ...ATTR_PATH, 'DCM', '7']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                    <Grid item md={4} >
                                        <CustomTextField
                                            id="dcm9"
                                            label="DCM 9"
                                            value={simulate.getIn([ ...ATTR_PATH, 'DCM', '8'])}
                                            path={[ ...ATTR_PATH, 'DCM', '8']}
                                            changeSimple={changeSimple}
                                            styles={styles}
                                            type="number"
                                        />
                                    </Grid>
                                </Fragment>
                            }
                        </Grid>
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel classes={{ root: styles.card }}>
                <ExpansionPanelSummary
                    classes={{ root: styles.header }}
                    expandIcon={<ExpandMore />}
                >
                    <div className={styles.full}>
                        <Header title="Satelite Configuration" styles={styles} />
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className={styles.full}>
                        <Grid container>
                            <Grid item md={6}>
                                <CustomParameter
                                    label='Mass'
                                    id="mass"
                                    path={[ ...ATTR_PATH, 'Mass', 'value']}
                                    unitPath={[ ...ATTR_PATH, 'Mass', 'Units']}
                                    unitOptions={[makeOption('Kg'), makeOption('Grams', 'Gm')]}
                                    styles={styles}
                                    changeSimple={changeSimple}
                                    value={simulate.getIn([ ...ATTR_PATH, 'Mass', 'value'])}
                                    unitValue={simulate.getIn([ ...ATTR_PATH, 'Mass', 'Units'])}
                                />
                            </Grid>
                            <Grid item md={6}>
                                <FormControl className={styles.formControl}>
                                    <InputLabel htmlFor='momentOfInertiaUnit'>Moment Of Inertia Unit</InputLabel>
                                    <Select
                                        value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'units'])}
                                        onChange={e => changeSimple([ ...ATTR_PATH, 'MOI', 'units'], e.target.value)}
                                        inputProps={{
                                            name: 'momentOfInertiaUnit',
                                            id: 'momentOfInertiaUnit',
                                        }}
                                    >
                                        <MenuItem value='kgm2'> Kg m^2 </MenuItem>
                                        <MenuItem value='m2'> gram m^2 </MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item md={4}>
                            <FormControl component="fieldset" className={styles.formControl} row>
                                <FormLabel component="legend">Moment Of Inertia Ix</FormLabel>
                                <CustomTextField
                                    id="Ixx"
                                    label="Ixx"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Ixx'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Ixx']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                                <CustomTextField
                                    id="Ixy"
                                    label="Ixy"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Ixy'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Ixy']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                                <CustomTextField
                                    id="Ixz"
                                    label="Ixz"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Ixz'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Ixz']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                            </FormControl>
                            </Grid>
                            <Grid item md={4}>
                            <FormControl component="fieldset" className={styles.formControl} row>
                                <FormLabel component="legend">Moment Of Inertia Iy</FormLabel>
                                <CustomTextField
                                    id="Iyx"
                                    label="Iyx"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Iyx'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Iyx']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                                <CustomTextField
                                    id="Iyy"
                                    label="Iyy"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Iyy'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Iyy']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                                <CustomTextField
                                    id="Iyz"
                                    label="Iyz"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Iyz'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Iyz']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                            </FormControl>
                            </Grid>
                            <Grid item md={4}>
                            <FormControl component="fieldset" className={styles.formControl} row>
                            <FormLabel component="legend">Moment Of Inertia IZ</FormLabel>
                                <CustomTextField
                                    id="Izx"
                                    label="Izx"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Izx'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Izx']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                                <CustomTextField
                                    id="Izy"
                                    label="Izy"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Izy'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Izy']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />
                                <CustomTextField
                                    id="Izz"
                                    label="Izz"
                                    value={simulate.getIn([ ...ATTR_PATH, 'MOI', 'value', 'Izz'])}
                                    path={[ ...ATTR_PATH, 'MOI', 'value', 'Izz']}
                                    changeSimple={changeSimple}
                                    styles={styles}
                                    type="number"
                                />

                            </FormControl>
                            </Grid>

                        </Grid>
                    </div >
                </ExpansionPanelDetails >
            </ExpansionPanel >
            <div className={classNames('d-flex')}>
                <Button
                    color="primary"
                    variant="contained"
                    classes={{ root: styles.button }}
                >Save</Button>
                <Button
                    color="primary"
                    variant="contained"
                    classes={{ root: styles.button }}
                    onClick={handleRun}
                >Run Simulation</Button>
                <Link to="simulate/new">new sim</Link>
            </div>
        </div >
    )
}

const mapStateToProps = state => ({
    simulate: state.simulate
});

const mapDispatchToProps = {
    changeAxis,
    changeUnit,
    createSimulation,
    changeSimple,
    changeTab
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Parameters))