
import Dashboard from 'Routes/dashboard';
import Crm from 'Routes/crm';


export default [
   {
      path: 'dashboard',
      component: Dashboard
   },
   {
      path: 'crm',
      component: Crm
   }
]